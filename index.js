/** BÀI TẬP 1 TÍNH TIỀN LƯƠNG NHÂN VIÊN
 * input : nhập số ngày làm
 *

 * step:
 * s1: tạo biến chứa tiền lương 1 ngày = 100.000
 * s2: tạo biến nhập số ngày làm
 * s3: ap dụng công thức lương= lương 1 ngày * số ngày làm
 
 * output: lương
*/
var luongNgay = 100000;
var soNgaylam = 28;
Luong = luongNgay * soNgaylam;

console.log("Lương= ", Luong);

/* BÀI TẬP 2: TÍNH GIÁ TRỊ TRUNG BÌNH
*
*input : nhập 5 số thực


step : 
s1: tạo 5 biến chứa 5 số thực
s2: tính trung bình 5 số thực =tổng 5 số / 5


output :xuất ra màn hình giá trị trung bình
*/

var a = 1;
var b = 2;
var c = 3;
var d = 4;
var e = 5;

avg = (a + b + c + d + e) / 5;
console.log("Giá trị trung bình = ", avg);

/**BAI TAP 3 : QUY ĐỔI TIỀN
 *
 *input: nhập số tiền USD
 *
 * step:
 * s1: tạo biến chứa giá trị Giá USD = 23.500 VND
 * s2: tạo biến chứa số tiền nhập
 * s3: giá quy đổi = giá usd * số tiền nhập
 *
 * output : xuất ra giá tiền sau quy đổi VND
 */

var giaUsd = 235000;
var soTien = 2;

giaQuydoi = giaUsd * soTien;
console.log("Số tiền quy đổi= ", giaQuydoi, "VND");

/* BAI TẬP 4 : TÍNH DIỆN TÍCH - CHU VI HÌNH CN
 *
 * input : nhập chiều dài và chiều rộng
 *
 * step:
 * s1: tạo biến chứa giá trị chiều dài và chiều rộng
 * s2: công thức diện tích = dài * rộng
 * công thức chu vi = (dài + rộng ) *2
 * output :  xuất ra giá trị diện tích và chu vi
 */

var dai = 10;
var rong = 5;
dienTich = dai * rong;
chuVi = (dai + rong) * 2;
console.log("Diện Tích = ", dienTich);
console.log("Chu Vi= ", chuVi);

/* BAI TAP 5: TÍNH TỔNG 2 KÝ SỐ
 * input: nhập 1 số có 2 chữ số
 *
 * step:
 * s1: tạo  biến chứa 1 số có 2 chữ số
 * s2: tính tổng 2 ký số của số vừa nhập
 * s3:tổng = so / 10 + so % 10
 * output: xuất ra tổng 2 ký số
 */

var so = 46;
soHangchuc = Math.floor(so / 10);
soHangdv = so % 10;

tongKyso = soHangchuc + soHangdv;
console.log("Tổng 2 ký số = ", tongKyso);
